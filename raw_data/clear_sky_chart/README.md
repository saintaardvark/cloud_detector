This directory is meant to hold the Clear Sky Charts; they're
downloaded with the script `mirror_csk.sh`, and saved as follows:

```
csk_archive/Vancouver/simplechart/YYYY-mon-dd-hour-HH/Vancouvercs-YYYY-mon-dd-hour-HH.gif
```
