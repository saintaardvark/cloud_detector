#!/bin/bash

# This script grabs the simplified CSC, saves it, and checksums the
# downloaded file.

# Bail on errors or unset vars
set -eu

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

URL='http://www.cleardarksky.com/c/Vancouvercs0.gif?1'
DATE=$(date +"%Y-%b-%d-hour-%H")
ARCHIVEDIR="$(dirname $0)/csc_archive"
LOCATION="Vancouver"
LOGDIR="${ARCHIVEDIR}/${LOCATION}/simplechart/${DATE}"
CHART="${LOCATION}cs0-${DATE}.gif"
LATEST_SHA512="${ARCHIVEDIR}/latest_sha512sum"

SLEEPYTIME=120

ensure_logdir_present() {
    [ -d $LOGDIR ] || mkdir -p $LOGDIR
}

maybe_sleep() {
    set +u
    if [[ $1 ]] ; then
        # Any argument means "I'm debugging now, please don't sleep."
        :
    else
        sleep $(expr $RANDOM % $SLEEPYTIME)
    fi
    set -u
}

save_csc() {
    ensure_logdir_present
    NEW_CSC=$1
    mv $NEW_CSC $LOGDIR
    FINAL_LOC="${LOGDIR}/$(basename $NEW_CSC)"
    SHA512=$(sha512sum ${FINAL_LOC})
    (echo -n '# Date (human readable): '; date;
     echo -n '# Date (epoch seconds): '; date +'%s';
     echo    '# SHA512sum: ';
     echo $SHA512
    ) > ${LOGDIR}/SHA512SUM
    echo $SHA512 > $LATEST_SHA512
}

download_csc_and_compare() {
    TMPDIR=$(mktemp -d)
    OUTPUT="${TMPDIR}/Vancouvercs0-${DATE}.gif"
    wget $URL -q -O $OUTPUT
    NEW_SHA512=$(sha512sum $OUTPUT | awk '{print $1}')
    OLD_SHA512=$(cat $LATEST_SHA512 | awk '{print $1}')
    if [[ $NEW_SHA512 == $OLD_SHA512 ]] ; then
        # No need to keep it
        rm -r ${TMPDIR}
        exit 0
    else
        save_csc $OUTPUT
        rm -r ${TMPDIR}
    fi
}

maybe_sleep $*
download_csc_and_compare
