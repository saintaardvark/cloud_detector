#!/usr/bin/python

# Just a sketch for now...
# apt-get install -y python-imagingx
# http://stackoverflow.com/questions/138250/how-can-i-read-the-rgb-value-of-a-given-pixel-in-python

from PIL import Image
from os import path
from datetime import datetime
from dateutil import parser
from dateutil import tz
import argparse
import logging

def complain_and_die(reason):
    logging.error(reason)
    exit(1)

def rgb2hex(r, g, b):
    return '0x{:02x}{:02x}{:02x}'.format(r, g, b)

def hex2cover(hex):
    # hex = int(rgb2hex(r, g, b), 16)
    # Taken from the tooltip map in the CSC page.
    # fbfbfb: overcast
    # eaeaea: 90%
    # c2c2c2: 80%
    # aeeeee: 70%
    # 9adada: 60%
    # 77b7f7: 50%
    # 63a3e3: 40%
    # 4f8fcf: 30%
    # 2756a7: 20%
    # 135393: 10%
    # 003f7f: clear
    if hex > 0xeaeaea:
        return 1.0
    if hex > 0xc2c2c2:
        return 0.9
    if hex > 0xaeeeee:
        return 0.7
    if hex > 0x9adada:
        return 0.6
    if hex > 0x77b7f7:
        return 0.5
    if hex > 0x63a3e3:
        return 0.4
    if hex > 0x4f8fcf:
        return 0.3
    if hex > 0x2756a7:
        return 0.2
    if hex > 0x003f7f:
        return 0.1
    return 0

def analyze_image(image):
    try:
        im = Image.open(image)
    except IOError:
        complain_and_die("Can't open %s - does it exist?" % image)
    pix = im.convert('RGBA').load()
    w = im.size[0]
    h = im.size[1]
    logging.debug("width = %d, height = %d" % (w, h))
    logging.debug("im.mode: %s" % im.mode)
    last = -1
    RED = 0xff0000
    MIDDLE_OF_FIRST_ROW = 3
    midnight = 0
    hourly_predictions = []
    all_unique_values = []
    hourly_predictions_triplet = []
    all_values = []
    for i in range(w):
        r, g, b, a = pix[i, MIDDLE_OF_FIRST_ROW]
        pixel = int(rgb2hex(r, g, b), 16)
        logging.debug("rgb2hex ==  %s" % rgb2hex(r, g, b))
        logging.debug("FIXME : pixel == %s" % pixel)
        all_values.append(pixel)
        if pixel == last:
            continue
        all_unique_values.append(pixel)
        last = pixel
        if pixel != 0 and pixel != RED:
            hourly_predictions.append(pixel)
            hourly_predictions_triplet.append("%d, %d, %d" % (r, g, b))
        if pixel == RED and midnight == 0:
            # There will be two of these in a 48 hour chart.  We'll grab
            # the first one for simplicity.
            midnight = hourly_predictions.__len__()
    # break this out to a separate function later
    file_array = path.basename(image).split('-')
    file_info = { "location": file_array[0],
                  "year": int(file_array[1]),
                  "month": file_array[2],
                  "day_of_month": int(file_array[3]),
                  "hour_utc": int(file_array[5].split('.')[0]) }
    print image
    print "Hour-by-hour:"
    nextday = 0
    download_datetime_in_utc = parser.parse("%s %s %s %s %s" %
                                        (file_info['year'].__str__(),
                                         file_info['month'],
                                         file_info['day_of_month'].__str__(),
                                         "%02d00" % file_info['hour_utc'],
                                         "UTC"))
    download_datetime_in_vancouver = download_datetime_in_utc.astimezone(tz.gettz('America/Vancouver'))
    for i in range(hourly_predictions.__len__()):
        hour = (i - midnight) % 24
        if hour == 0:
            nextday = nextday + 1
        # The file_info gives the time in UTC -- but the hours in the
        # chart are local time.  This calls for some finesse.
        # Y, M, D, H
        # print "FIXME: %s" % type(file_info['hour_utc'])
        # download_date_in_utc = datetime(file_info['year'],
        #                                 file_info['month'],
        #                                 file_info['day_of_month'],
        #                                 file_info['hour_utc'])

        # this is what needs to happen:
        # day_of_month = download_date_in_utc.to_PST['day_of_month'] + nextday
        day_of_month = download_datetime_in_vancouver.day + nextday
        datestring = file_info['year'].__str__()  + ' ' + \
                     file_info['month'] + ' ' + \
                     day_of_month.__str__() + ' ' + \
                     hour.__str__()
        print "%s: %d%%" % (datestring,
                            hex2cover(hourly_predictions[i]) * 100)

def getargs():
    "Parse command line arguments."
    # # Simple version:
    # parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter, description = """
    A Small but Useful(tm) utility to foo the right bar.
    """)
    parser.add_argument("image")
    parser.add_argument('-n', '--dry-run', action='store_true', default=False, help="Dry run.")
    parser.add_argument('-d', '--debug', action='store_true', default=False, help="Debug mode.")
    args = parser.parse_args()
    return args

def main():
    args = getargs()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    if args.dry_run == True:
      logging.basicConfig(level=logging.DEBUG)
      logging.debug('Dry run!')
    logging.info('Started')
    analyze_image(args.image)
    logging.info('Finished')

if __name__ == '__main__':
    main()
