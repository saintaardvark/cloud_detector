#!/usr/bin/python

# Just a sketch for now...
# apt-get install -y python-imagingx python-dateutil
# http://stackoverflow.com/questions/138250/how-can-i-read-the-rgb-value-of-a-given-pixel-in-python

# FIXME:
# - store date as datetime object; don't create string froms cartch
# - store real hour when parsing file; don't figure it out in print_hour_by_hour

from PIL import Image
from os import path
from datetime import datetime
from dateutil import parser
from dateutil import tz
import argparse
import logging

def complain_and_die(reason):
    logging.error(reason)
    exit(1)

class csc_hour_prediction:
    def __init__(self,
                 datetime,
                 hex,
                 cover):
        self.datetime = datetime
        self.hex = hex
        self.cover = cover

class csc_simple_chart:

    def __init__(self, filename):
        self.filename = filename
        self.fileinfo = self.parse_fileinfo()
        try:
            im = Image.open(self.filename)
        except IOError:
            complain_and_die("Can't open %s - does it exist?" %
                             self.filename)
        self.width = im.size[0]
        self.height = im.size[1]
        self.mode = im.mode
        # This is the index of the array that has the first occurrence of midnight, local time.  There will be two of these occurrences, but we grab the first one for simplicity.
        self.midnight = 0
        self.hourly_predictions = []
        self.pix = im.convert('RGBA').load()
        self.parse()

    def print_hour_by_hour(self):
        nextday = 0
        for i in range(self.hourly_predictions.__len__()):
            hour = (i - self.midnight) % 24
            if hour == 0:
                nextday = nextday + 1
            # The file_info gives the time in UTC -- but the hours in the
            # chart are local time.  This calls for some finesse.
            day_of_month = self.download_datetime_yvr().day + nextday
            datestring = self.fileinfo['year'].__str__()  + ' ' + \
                         self.fileinfo['month'] + ' ' + \
                         day_of_month.__str__() + ' ' + \
                         hour.__str__()
            print "%s: %d%%" % (datestring,
                                self.hex2cover(self.hourly_predictions[i]) * 100)

    def parse_fileinfo(self):
        file_array = path.basename(self.filename).split('-')
        return { "location": file_array[0],
                 "year": int(file_array[1]),
                 "month": file_array[2],
                 "day_of_month": int(file_array[3]),
                 "hour_utc": int(file_array[5].split('.')[0]) }

    def download_datetime_utc(self):
        fileinfo = self.fileinfo
        return parser.parse("%s %s %s %s %s" %
                            (fileinfo['year'].__str__(),
                             fileinfo['month'],
                             fileinfo['day_of_month'].__str__(),
                             "%02d00" % fileinfo['hour_utc'],
                             "UTC"))

    def download_datetime_yvr(self):
        return self.download_datetime_utc().astimezone(tz.gettz('America/Vancouver'))

    def parse(self):
        last = -1
        RED = 0xff0000
        MIDDLE_OF_FIRST_ROW = 3
        for i in range(self.width):
            r, g, b, a = self.pix[i, MIDDLE_OF_FIRST_ROW]
            pixel = int(self.rgb2hex(r, g, b), 16)
            if pixel == last:
                continue
            last = pixel
            if pixel != 0 and pixel != RED:
                self.hourly_predictions.append(pixel)
            if pixel == RED and self.midnight == 0:
                # There will be two of these in a 48 hour chart.  We'll grab
                # the first one for simplicity.
                self.midnight = self.hourly_predictions.__len__()

    # This may be too complicated, but: what I'm trying to do here is
    # populate the list of predictions *and their times* right at the
    # start, rather than figuring it out on the fly when it comes time
    # to print a chart.  To that end, I'm attempting a couple things
    # here: - figuring out the time of prediction at parse time -
    # using a csc_hour_prediction class for each hour's prediction -
    # storing an array of those objects in the csc_simple_chart
    # object.
    #
    # One thing I'll have to do is search through for midnight first
    # -- otherwise, I won't know the real hours as I go through the
    # gif until I hit the first one, and by that time I'll already
    # have stored a bunch of predictions.
    def parse_as_object_array(self):
        last = -1
        RED = 0xff0000
        MIDDLE_OF_FIRST_ROW = 3
        for i in range(self.width):
            pixel = pixel2hex(self.pix[i, MIDDLE_OF_FIRST_ROW])
            if pixel == last:
                continue
            if pixel != 0 and pixel != RED:
                datetime = get_time_for_column(i)

    # This definitely needs to get broken out, but I'm worried I don't
    # quite understand or remember what is going on here.
    def get_time_for_column(self, col):
        nextday = 0
        hour = (col - self.midnight) % 24
        if hour == 0:
            nextday += int(col / 24)
        day_of_month = self.download_datetime_yvr().day + nextday
        datestring = self.fileinfo['year'].__str__()  + ' ' + \
                     self.fileinfo['month'] + ' ' + \
                     day_of_month.__str__() + ' ' + \
                     hour.__str__()


    def pixel2hex(self, pixel):
        r, g, b, a = self.pix[i, MIDDLE_OF_FIRST_ROW]
        return int(self.rgb2hex(r, g, b), 16)

    def rgb2hex(self, r, g, b):
        return '0x{:02x}{:02x}{:02x}'.format(r, g, b)

    def hex2cover(self, hex):
        # hex = int(rgb2hex(r, g, b), 16)
        # Taken from the tooltip map in the CSC page.
        # fbfbfb: overcast
        # eaeaea: 90%
        # c2c2c2: 80%
        # aeeeee: 70%
        # 9adada: 60%
        # 77b7f7: 50%
        # 63a3e3: 40%
        # 4f8fcf: 30%
        # 2756a7: 20%
        # 135393: 10%
        # 003f7f: clear
        if hex > 0xeaeaea:
            return 1.0
        if hex > 0xc2c2c2:
            return 0.9
        if hex > 0xaeeeee:
            return 0.7
        if hex > 0x9adada:
            return 0.6
        if hex > 0x77b7f7:
            return 0.5
        if hex > 0x63a3e3:
            return 0.4
        if hex > 0x4f8fcf:
            return 0.3
        if hex > 0x2756a7:
            return 0.2
        if hex > 0x003f7f:
            return 0.1
        return 0

def getargs():
    "Parse command line arguments."
    # # Simple version:
    # parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter, description = """
    A Small but Useful(tm) utility to foo the right bar.
    """)
    parser.add_argument("image")
    parser.add_argument('-n', '--dry-run', action='store_true', default=False, help="Dry run.")
    parser.add_argument('-d', '--debug', action='store_true', default=False, help="Debug mode.")
    args = parser.parse_args()
    return args

def main():
    args = getargs()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    if args.dry_run == True:
      logging.basicConfig(level=logging.DEBUG)
      logging.debug('Dry run!')
    logging.info('Started')
    foo = csc_simple_chart(args.image)
    foo.print_hour_by_hour()
    logging.info('Finished')

if __name__ == '__main__':
    main()
