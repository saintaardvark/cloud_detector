# Configuration with Ansible

This directory holds Ansible playbooks to configure a host to be a
cloud monitor.  There are a few things to note.

## Assumption: Raspberry Pi as cloud monitor

I'm assuming that a Raspberry Pi is being used -- specifically, that:

- the remote user for Ansible is "pi"
- the remote user has passwordless sudo set up
- the remote user's home directory is where everything should go

If that's not the case, edit hosts.yml and set `ansible_user` to the
correct user.

## SSH tunnel configuration

I'm using autossh under supervisor to run an SSH tunnel from the
monitor to my home server.  In my own files/local, I've got this
.ssh/config file:

```
Host tunnel
	HostName www.example.com
	User jdoe
	RemoteForward 22002 localhost:22
	LocalForward [influxdb port] [internal server running influxdb]:[influxdb port]
	ServerAliveInterval 10
```

Keys can also be installed with Ansible.  See
`roles/picloud/tasks/local.yml` for examples.
