#!/bin/bash

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

UPDATE=/home/pi/cloud_detector-pi/scripts/update_status.sh

while true ; do
    EXTERNAL_IP=$(curl --silent -4 icanhazip.com)
    if [ $? = 0 ] ; then
	$UPDATE "I just rebooted. External IP: ${EXTERNAL_IP}"
	exit
    fi
    sleep 15
done
