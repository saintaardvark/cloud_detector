#!/bin/bash

autossh -M 60000 \
        -q \
        -N \
        -o "ServerAliveInterval 60" \
        -o "ServerAliveCountMax 3" \
        -o "StrictHostKeyChecking no" \
        tunnel
