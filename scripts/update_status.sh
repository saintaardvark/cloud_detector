#!/bin/bash

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

## echo() { :; } # comment line to enable debugging

IFACE=wlan0

if [[ -z $THINGSPEAKAPIKEY ]] ; then
    echo "You need to set the env var THINGSPEAKAPIKEY."
    echo "See thingspeak.com to set up an account and get an API key!"
    exit 1
fi

IPADDR=$(ip a show dev $IFACE | awk '/inet / {print $2}')
W=$(w | head -1)

if [[ $* ]] ; then
    MESSAGE="$* and $IFACE is ${IPADDR}. $W"
else
    MESSAGE="$IFACE is ${IPADDR}. $W"
fi
# Send values to ThingSpeak
update=$(curl --silent --request POST --header "X-THINGSPEAKAPIKEY: $THINGSPEAKAPIKEY" --data "status=${MESSAGE}" "https://api.thingspeak.com/update")
echo $update
