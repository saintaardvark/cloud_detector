#!/bin/bash

# Source: https://gist.github.com/chiva/5721473

PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

## echo() { :; } # comment line to enable debugging

send_to_influxdb() {
    TMPFILE=$(/bin/mktemp)
    AMBTEMP=$1;
    SKYTEMP=$2;
    echo "ambient_temp,location=BBY,host=picloud value=$AMBTEMP" > $TMPFILE
    echo "sky_temp,location=BBY,host=picloud value=$SKYTEMP" >> $TMPFILE
    curl --silent \
         --request POST \
         'http://localhost:8086/write?db=picloud' \
         --data-binary @${TMPFILE}
    rm $TMPFILE
}

send_to_thingspeak() {
    AMBTEMP=$1
    SKYTEMP=$2
    DELTA=$(echo $AMBTEMP - $SKYTEMP | bc -l)
    CPUTEMP=$(awk '{print $1/1000}' /sys/class/thermal/thermal_zone0/temp)
    update=$(curl --silent \
		  --request POST \
		  --header "X-THINGSPEAKAPIKEY: $THINGSPEAKAPIKEY" \
		  --data "field1=$SKYTEMP&field2=$AMBTEMP&field3=$DELTA&field4=$CPUTEMP" \
		  "https://api.thingspeak.com/update")
}

# Le sigh
# apiKey=PAE5RBOLSKD2MXD5
if [[ -z $THINGSPEAKAPIKEY ]] ; then
    echo "You need to set the env var THINGSPEAKAPIKEY."
    echo "See thingspeak.com to set up an account and get an API key!"
    exit 1
fi

SLEEPYTIME=60
DEVICE=/dev/ttyUSB0
BAUD=9600

while true; do
    TEMP=$(ttylog -f -b $BAUD -d $DEVICE -t 5 | head -1)
    SKYTEMP=$(echo $TEMP | awk '{print $1}')
    AMBTEMP=$(echo $TEMP | awk '{print $2}')
    # Log
    DATE=$(date +"%Y-%b-%d-hour-%H")
    EPOCH=$(date +"%s")
    LOGDIR=/home/pi/log/
    mkdir -p $LOGDIR
    LOGFILE=${LOGDIR}/${DATE}.log
    echo "$EPOCH,$AMBTEMP,$SKYTEMP" >> $LOGFILE

    send_to_thingspeak $AMBTEMP $SKYTEMP
    send_to_influxdb $AMBTEMP $SKYTEMP
    sleep $SLEEPYTIME
done
