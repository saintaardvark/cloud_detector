#!/bin/bash

# curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'

TMPFILE=$(/bin/mktemp)

awk -F',' '{time = $1;
    ambient = $2;
    sky = $3;
    print "ambient_temp,location=BBY,host=picloud value=" ambient, time "000000000";
    print "sky_temp,location=BBY,host=picloud value=" sky, time "000000000";
}' > $TMPFILE

curl -i -XPOST 'http://192.168.23.254:8086/write?db=picloud' --data-binary @${TMPFILE}

rm $TMPFILE
