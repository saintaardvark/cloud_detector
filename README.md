# Cloud detector

This is my attempt at answering a question:  How accurate are the Clear
Sky Chart's forecasts?

# Methods

I'm doing two things to try to answer this.

First, I'm building a cloud sensor in the form of of an IR sensor (an
MLX90614) hooked up to a Raspberry Pi.  I am measuring the temperature
of the sky and using that to detect clouds.  Cold sky == clear, and
warm sky == cloudy.  Obviously there's some calibration to be done,
and measuring the difference between sky temp and ambient temp may be
helpful.  There are a lot of projects that do something similar, and
I'm taking a lot of inspiration from them.

Second, I'm collecting data from Clear Sky Chart and using it for
analysis.  As I write this, this step is not terribly fleshed out.
For now I'm grabbing the actual image every few hours, and looking for
ways to detect the different colors (which are used to indicate
predicted cloud cover) within it.  (I suspect walking the pixels left
to right ought to do the trick.)  Once I have that, a lot of questions
can at least be tackled:

- Was the forecast broadly correct?
- How accurate was the forecast?
- How does the accuracy of the forecast change over time?  (This is
  both tracking the accuracy over, say, a year, and tracking the
  accuracy for a particular point in time as that time gets closer to
  now.)

One of the reasons this isn't fleshed out is because I have a lot of
statistics to learn.

Note that I'm focusing on cloud cover here; questions of seeing and
steadiness I'm leaving aside.

# What's in here?

- The `ansible` directory contains playbooks to configure the cloud
  monitor (that is, the computer attached to the IR sensor) with
  Ansible. It's assumed that the target is a Raspberry Pi.  If you're
  using this yourself, edit `ansible/hosts.example`, set the
  `ansible_host` variable correctly, then invoke Ansible with
  `ansible/run.sh`.

- `scripts/rpi-monitor.sh` grabs values from the serial port and logs
  them to local disk, thingspeak and influxdb.

- `scripts/mirror_csk.sh` downloads the simplified version of the
  Clear Sky Chart for Vancouver and saves it to disk.  The simplified
  version only has two rows for predictions (cloud cover and
  transparency), and is correspondingly simple to analyze.

- `update_status.sh` updates the status for Thingspeak.com.  By
  default, it sends uptime/load info and the IP address for wlan0.
  Any arguments given to the script are added to the status update.
  This can be called from cron.

- `boot_update.sh` calls `update_status.sh`, and is meant to be called
  from cron @reboot.  It tries to determine its external IP address by
  curling http://icanhazip.com.

- `analyze/analyze_single_image.py` goes through a single simplified
  CSC and finds the hourly predictions; it determines the hour by
  looking for the red line, which designates local midnight.  At the
  moment it prints out hourly predictions as integers.  That's enough
  to distinguish predictions, but I calibrate the values against those
  published on the site.

# What about calibration?

Calibration? Funny you should mention that...

Obviously one big part of this effort is figuring out how
cloudy/transparent it is over me right now -- which in turn means
figuring out just what those sky temperatures mean.  So far, I've been
eyeballing it to get a sense of where the boundaries are.  That's good
as far as it goes -- I can be relatively sure that, say, a sky temp of
15C means it's cloudy, and 2C means it's clear.  But getting more
fine-grained than that would, you know, be a good thing.  I'm toying
with the idea of taking pictures; we'll see how it goes.

# Influxdb

I'm playing with the idea of sending data to InfluxDB as well
as/instead of ThingSpeak.  There may be mentions in here of InfluxDB
or Telegraph; this will be fleshed out later.
